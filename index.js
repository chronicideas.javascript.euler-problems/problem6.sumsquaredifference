function getSumOfSquares(maxVal) {
  let sumOfSquares = 0;
  for(let i = 1; i <= maxVal; i++) {
    sumOfSquares = sumOfSquares + (i*i);
  }
  return sumOfSquares;
}

function getSquareOfSum(maxVal) {
  let sum = 0;
  for(let i = 1; i <= maxVal; i++) {
    sum = sum + i;
  }
  return sum * sum;
}

function getDifference(maxVal) {
  return getSquareOfSum(maxVal) - getSumOfSquares(maxVal);
}

module.exports.getDifference = getDifference;