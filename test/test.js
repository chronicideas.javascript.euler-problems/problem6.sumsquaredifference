const assert = require('assert');
const index = require('../index.js');

describe('Problem6', function() {
  it('The difference between the square of sums and the sum of squares is correct for the first 20 numbers', function() {
    assert.equal(index.getDifference(20), 41230);
  });
});
